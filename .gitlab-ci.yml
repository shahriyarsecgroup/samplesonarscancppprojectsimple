# A pipeline is composed of independent jobs that run scripts, grouped into stages.
# Stages run in sequential order, but jobs within stages run in parallel.

image: gcc
before_script:
  # Install build dependencies
  - echo CI_PROJECT_NAME is "$CI_PROJECT_NAME" 
  - echo CI_PROJECT_ID is "$CI_PROJECT_ID"
  - echo CI_PROJECT_PATH is "$CI_PROJECT_PATH"
  - echo CI_REGISTRY is "$CI_REGISTRY"
  - apt-get update && apt-get -y install cmake


variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task



# List of stages for jobs, and their order of execution.
stages:
  - build
  - test
  - deploy



get-sonar-binaries:
  stage: .pre
  cache:
    policy: push
    key: "${CI_COMMIT_SHORT_SHA}"
    paths:
      - build-wrapper/
      - sonar-scanner/
  script:
    # Download sonar-scanner
    - curl -sSLo ./sonar-scanner.zip 'https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.7.0.2747-linux.zip'
    - unzip -o sonar-scanner.zip
    - mv sonar-scanner-4.7.0.2747-linux sonar-scanner
    # Download build-wrapper
    - curl -sSLo ./build-wrapper-linux-x86.zip "${SONAR_HOST_URL}/static/cpp/build-wrapper-linux-x86.zip"
    - unzip -oj build-wrapper-linux-x86.zip -d ./build-wrapper
  # only:
  #   - merge_requests
  #   - main


build:
  stage: build
  cache:
    policy: pull-push
    key: "${CI_COMMIT_SHORT_SHA}"
    paths:
      - build-wrapper/
      - sonar-scanner/
      - bw-output/
      - build-cm/
  script:
      # Prepare the build system
    - mkdir build-cm && cd build-cm && cmake .. && cd ..
    # Run the build inside the build wrapper
    - NUMBER_OF_PROCESSORS=$(nproc --all)
    # Wraps the compilation with the Build Wrapper to generate configuration 
    # --> (used later by the SonarQube Scanner) into the "bw-output" folder
    - build-wrapper/build-wrapper-linux-x86-64 --out-dir bw-output cmake --build build-cm/




unit-test-job:   # This job runs in the test stage.
  stage: test    # It only starts when the job in the build stage completes successfully.
  script:
    - echo "Running unit tests... This will take about 60 seconds."
    - sleep 2
    - echo "Code coverage is 90%"


lint-test2-job:   # This job also runs in the test stage.
  stage: test    # It can run at the same time as unit-test-job (in parallel).
  script:
    - echo "Linting code... This will take about 10 seconds."
    - sleep 1
    - echo "No lint issues found."




deploy-job:      # This job runs in the deploy stage.
  stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
  environment: production
  script:
    - echo "Deploying application..."
    - echo "Application successfully deployed."




sonarcloud-check:
  stage: .post
  cache:
    policy: pull
    key: "${CI_COMMIT_SHORT_SHA}"
    paths:
      - build-wrapper/
      - sonar-scanner/
      - bw-output/
      - build-cm/
  script:
    - NUMBER_OF_PROCESSORS=$(nproc --all)
    - sonar-scanner/bin/sonar-scanner -X -Dsonar.host.url="${SONAR_HOST_URL}" -Dsonar.token="${SONAR_TOKEN}" -Dsonar.cfamily.build-wrapper-output=bw-output -Dsonar.cfamily.cache.enabled=true -Dsonar.cfamily.cache.path=${GITLAB_HOME}/.cfamily -Dsonar.cfamily.threads=${NUMBER_OF_PROCESSORS}
  allow_failure: false
  # only:
  #   - merge_requests
  #   - main
